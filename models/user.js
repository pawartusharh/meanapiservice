const mongoose = require('mongoose')

const Schema = mongoose.Schema
const userSchema = new Schema({
    name: String,
    email: String,
    phone: Number,
    topic: String,
    timePreferance: String,
    subscribe: Boolean,
    password: String,
    userName: String,
    address: Object,
    alternetEmails: Array
})
module.exports = mongoose.model('user', userSchema, 'users')