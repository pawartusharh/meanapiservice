require('dotenv').config()
const express = require('express');
const bodyParser = require('body-parser')
const api = require('./routes/api')
const app = express();
const port = process.env.port || 3001;
const cors = require('cors')

app.use(cors())
app.use(bodyParser.json())
app.use('/api', api)

// app.get('/pingme', (req, res) => {
//     res.send('Ping Received')
// })

app.listen(port, () => {
    console.log('server is running on port', port)
})


