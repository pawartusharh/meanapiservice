const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken')
const mongoose = require('mongoose')
const User = require('../models/user')

db = "mongodb+srv://tushar:pass@cluster0-zb39m.mongodb.net/test?retryWrites=true&w=majority";

mongoose.connect(db, err => {
    if(err){
        console.log("Error", err)
    } else {    
        console.log("Connected to mongodb.")
    }
})

function verifyToken(req,res,next){
    if(!req.headers.authorization){
        return res.status(401).send('Unauthorised request')
    }
    let token = req.headers.authorization.split(' ')[1]
    if(token === "null"){
        return res.status(401).send('Unauthorised request')
    }
    let payload = jwt.verify(token, process.env.SECRET_KEY)
    if(!payload){
        return res.status(401).send('Unauthorised request')
    }
    req.userid = payload.subject
    next()
}

router.post('/register', (req,res) => {
    let userData = req.body
    let user = new User(userData);
    user.save((error, registeredUser)=> {
        if (error) {
            console.log('Error response ', error)
        } else {
            let payload = {subject: userData.username}
            let token = jwt.sign(payload, process.env.SECRET_KEY)
            res.status(200).send({token})
            // res.status(200).send(registeredUser)
        }
    });
})

router.post('/login', (req,res)=> {
    let userData = req.body;

    User.findOne({email: userData.email}, (error, user) => {
        if (error) {
            console.log('Error ', error)
        } else {
            if (!user) {
                res.status(401).send('Invalid email.');
            } else if (user.password !== userData.password) {
                res.status(401).send('Invalid password.');
            } else {
                let payload = {subject: userData.username}
                let token = jwt.sign(payload, process.env.SECRET_KEY)
                res.status(200).send({token})    
                // res.status(200).send(user);
            }
        }
    })
})

router.get('/specialEvents', verifyToken, (req,res) => {
    let specialEvents = [
        {
          "_id": "1",
          "name": "Auto Expo Special",
          "description": "lorem ipsum",
          "date": "2012-04-23T18:25:43.511Z"
        },
        {
          "_id": "2",
          "name": "Auto Expo Special",
          "description": "lorem ipsum",
          "date": "2012-04-23T18:25:43.511Z"
        },
        {
          "_id": "3",
          "name": "Auto Expo Special",
          "description": "lorem ipsum",
          "date": "2012-04-23T18:25:43.511Z"
        },
        {
          "_id": "4",
          "name": "Auto Expo Special",
          "description": "lorem ipsum",
          "date": "2012-04-23T18:25:43.511Z"
        },{
          "_id": "5",
          "name": "Auto Expo Special",
          "description": "lorem ipsum",
          "date": "2012-04-23T18:25:43.511Z"
        },{
          "_id": "6",
          "name": "Auto Expo Special",
          "description": "lorem ipsum",
          "date": "2012-04-23T18:25:43.511Z"
        }
    ]    
    res.status(200).send(specialEvents)
})

router.get('/events', (req,res) => {
    let specialEvents = [
        {
            "_id": "1",
            "name": "Auto Expo Special",
            "description": "lorem ipsum",
            "date": "2012-04-23T18:25:43.511Z"
          },
          {
            "_id": "2",
            "name": "Auto Expo Special",
            "description": "lorem ipsum",
            "date": "2012-04-23T18:25:43.511Z"
          },
          {
            "_id": "3",
            "name": "Auto Expo Special",
            "description": "lorem ipsum",
            "date": "2012-04-23T18:25:43.511Z"
          },
          {
            "_id": "4",
            "name": "Auto Expo Special",
            "description": "lorem ipsum",
            "date": "2012-04-23T18:25:43.511Z"
          },{
            "_id": "5",
            "name": "Auto Expo Special",
            "description": "lorem ipsum",
            "date": "2012-04-23T18:25:43.511Z"
          },{
            "_id": "6",
            "name": "Auto Expo Special",
            "description": "lorem ipsum",
            "date": "2012-04-23T18:25:43.511Z"
          }
    ]    
    res.status(200).send(specialEvents)
})

router.post('/enroll', (req,res) => {
    res.status(200).send({'message':'data received'})
})

router.post('/createUser', (req,res) => {
    res.status(200).send({'message':'data received'})
})

module.exports = router;